import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import ProduitService from '@/entities/produit/produit.service';
import { IProduit } from '@/shared/model/produit.model';

import { IBoutique, Boutique } from '@/shared/model/boutique.model';
import BoutiqueService from './boutique.service';

const validations: any = {
  boutique: {
    idBoutique: {
      required,
      numeric,
    },
  },
};

@Component({
  validations,
})
export default class BoutiqueUpdate extends Vue {
  @Inject('boutiqueService') private boutiqueService: () => BoutiqueService;
  @Inject('alertService') private alertService: () => AlertService;

  public boutique: IBoutique = new Boutique();

  @Inject('produitService') private produitService: () => ProduitService;

  public produits: IProduit[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.boutiqueId) {
        vm.retrieveBoutique(to.params.boutiqueId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.boutique.id) {
      this.boutiqueService()
        .update(this.boutique)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.boutique.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.boutiqueService()
        .create(this.boutique)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.boutique.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveBoutique(boutiqueId): void {
    this.boutiqueService()
      .find(boutiqueId)
      .then(res => {
        this.boutique = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.produitService()
      .retrieve()
      .then(res => {
        this.produits = res.data;
      });
  }
}
