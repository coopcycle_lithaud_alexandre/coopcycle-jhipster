package polytech.info.gl.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import polytech.info.gl.service.dto.BoutiqueDTO;

/**
 * Service Interface for managing {@link polytech.info.gl.domain.Boutique}.
 */
public interface BoutiqueService {
    /**
     * Save a boutique.
     *
     * @param boutiqueDTO the entity to save.
     * @return the persisted entity.
     */
    BoutiqueDTO save(BoutiqueDTO boutiqueDTO);

    /**
     * Updates a boutique.
     *
     * @param boutiqueDTO the entity to update.
     * @return the persisted entity.
     */
    BoutiqueDTO update(BoutiqueDTO boutiqueDTO);

    /**
     * Partially updates a boutique.
     *
     * @param boutiqueDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<BoutiqueDTO> partialUpdate(BoutiqueDTO boutiqueDTO);

    /**
     * Get all the boutiques.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BoutiqueDTO> findAll(Pageable pageable);

    /**
     * Get the "id" boutique.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BoutiqueDTO> findOne(Long id);

    /**
     * Delete the "id" boutique.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
