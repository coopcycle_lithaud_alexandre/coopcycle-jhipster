import { IPanier } from '@/shared/model/panier.model';
import { IBoutique } from '@/shared/model/boutique.model';

export interface IProduit {
  id?: number;
  idProduit?: number;
  nomProduit?: string;
  prix?: number;
  estComposeDes?: IPanier[] | null;
  boutiques?: IBoutique[] | null;
}

export class Produit implements IProduit {
  constructor(
    public id?: number,
    public idProduit?: number,
    public nomProduit?: string,
    public prix?: number,
    public estComposeDes?: IPanier[] | null,
    public boutiques?: IBoutique[] | null
  ) {}
}
