/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import BoutiqueUpdateComponent from '@/entities/boutique/boutique-update.vue';
import BoutiqueClass from '@/entities/boutique/boutique-update.component';
import BoutiqueService from '@/entities/boutique/boutique.service';

import ProduitService from '@/entities/produit/produit.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Boutique Management Update Component', () => {
    let wrapper: Wrapper<BoutiqueClass>;
    let comp: BoutiqueClass;
    let boutiqueServiceStub: SinonStubbedInstance<BoutiqueService>;

    beforeEach(() => {
      boutiqueServiceStub = sinon.createStubInstance<BoutiqueService>(BoutiqueService);

      wrapper = shallowMount<BoutiqueClass>(BoutiqueUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          boutiqueService: () => boutiqueServiceStub,
          alertService: () => new AlertService(),

          produitService: () =>
            sinon.createStubInstance<ProduitService>(ProduitService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.boutique = entity;
        boutiqueServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(boutiqueServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.boutique = entity;
        boutiqueServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(boutiqueServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundBoutique = { id: 123 };
        boutiqueServiceStub.find.resolves(foundBoutique);
        boutiqueServiceStub.retrieve.resolves([foundBoutique]);

        // WHEN
        comp.beforeRouteEnter({ params: { boutiqueId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.boutique).toBe(foundBoutique);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
