package polytech.info.gl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Produit.
 */
@Entity
@Table(name = "produit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_produit", nullable = false, unique = true)
    private Long idProduit;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    @Column(name = "nom_produit", nullable = false)
    private String nomProduit;

    @NotNull
    @Column(name = "prix", nullable = false)
    private Long prix;

    @ManyToMany
    @JoinTable(
        name = "rel_produit__est_compose_de",
        joinColumns = @JoinColumn(name = "produit_id"),
        inverseJoinColumns = @JoinColumn(name = "est_compose_de_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "produits" }, allowSetters = true)
    private Set<Panier> estComposeDes = new HashSet<>();

    @OneToMany(mappedBy = "produit")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "produit" }, allowSetters = true)
    private Set<Boutique> boutiques = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Produit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdProduit() {
        return this.idProduit;
    }

    public Produit idProduit(Long idProduit) {
        this.setIdProduit(idProduit);
        return this;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public String getNomProduit() {
        return this.nomProduit;
    }

    public Produit nomProduit(String nomProduit) {
        this.setNomProduit(nomProduit);
        return this;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public Long getPrix() {
        return this.prix;
    }

    public Produit prix(Long prix) {
        this.setPrix(prix);
        return this;
    }

    public void setPrix(Long prix) {
        this.prix = prix;
    }

    public Set<Panier> getEstComposeDes() {
        return this.estComposeDes;
    }

    public void setEstComposeDes(Set<Panier> paniers) {
        this.estComposeDes = paniers;
    }

    public Produit estComposeDes(Set<Panier> paniers) {
        this.setEstComposeDes(paniers);
        return this;
    }

    public Produit addEstComposeDe(Panier panier) {
        this.estComposeDes.add(panier);
        panier.getProduits().add(this);
        return this;
    }

    public Produit removeEstComposeDe(Panier panier) {
        this.estComposeDes.remove(panier);
        panier.getProduits().remove(this);
        return this;
    }

    public Set<Boutique> getBoutiques() {
        return this.boutiques;
    }

    public void setBoutiques(Set<Boutique> boutiques) {
        if (this.boutiques != null) {
            this.boutiques.forEach(i -> i.setProduit(null));
        }
        if (boutiques != null) {
            boutiques.forEach(i -> i.setProduit(this));
        }
        this.boutiques = boutiques;
    }

    public Produit boutiques(Set<Boutique> boutiques) {
        this.setBoutiques(boutiques);
        return this;
    }

    public Produit addBoutique(Boutique boutique) {
        this.boutiques.add(boutique);
        boutique.setProduit(this);
        return this;
    }

    public Produit removeBoutique(Boutique boutique) {
        this.boutiques.remove(boutique);
        boutique.setProduit(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Produit{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", nomProduit='" + getNomProduit() + "'" +
            ", prix=" + getPrix() +
            "}";
    }
}
