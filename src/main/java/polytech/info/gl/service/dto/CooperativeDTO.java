package polytech.info.gl.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;
import polytech.info.gl.domain.enumeration.Role;

/**
 * A DTO for the {@link polytech.info.gl.domain.Cooperative} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CooperativeDTO implements Serializable {

    private Long id;

    @NotNull
    private Role directeur;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getDirecteur() {
        return directeur;
    }

    public void setDirecteur(Role directeur) {
        this.directeur = directeur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CooperativeDTO)) {
            return false;
        }

        CooperativeDTO cooperativeDTO = (CooperativeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, cooperativeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CooperativeDTO{" +
            "id=" + getId() +
            ", directeur='" + getDirecteur() + "'" +
            "}";
    }
}
