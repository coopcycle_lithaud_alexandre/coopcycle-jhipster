import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import UtilisateurService from './utilisateur/utilisateur.service';
import CooperativeService from './cooperative/cooperative.service';
import BoutiqueService from './boutique/boutique.service';
import ProduitService from './produit/produit.service';
import PanierService from './panier/panier.service';
import CoursesService from './courses/courses.service';
import PaiementService from './paiement/paiement.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('utilisateurService') private utilisateurService = () => new UtilisateurService();
  @Provide('cooperativeService') private cooperativeService = () => new CooperativeService();
  @Provide('boutiqueService') private boutiqueService = () => new BoutiqueService();
  @Provide('produitService') private produitService = () => new ProduitService();
  @Provide('panierService') private panierService = () => new PanierService();
  @Provide('coursesService') private coursesService = () => new CoursesService();
  @Provide('paiementService') private paiementService = () => new PaiementService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
