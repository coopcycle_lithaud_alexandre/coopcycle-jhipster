import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Courses e2e test', () => {
  const coursesPageUrl = '/courses';
  const coursesPageUrlPattern = new RegExp('/courses(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const coursesSample = {};

  let courses;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/courses+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/courses').as('postEntityRequest');
    cy.intercept('DELETE', '/api/courses/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (courses) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/courses/${courses.id}`,
      }).then(() => {
        courses = undefined;
      });
    }
  });

  it('Courses menu should load Courses page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('courses');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Courses').should('exist');
    cy.url().should('match', coursesPageUrlPattern);
  });

  describe('Courses page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(coursesPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Courses page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/courses/new$'));
        cy.getEntityCreateUpdateHeading('Courses');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursesPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/courses',
          body: coursesSample,
        }).then(({ body }) => {
          courses = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/courses+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/courses?page=0&size=20>; rel="last",<http://localhost/api/courses?page=0&size=20>; rel="first"',
              },
              body: [courses],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(coursesPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Courses page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('courses');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursesPageUrlPattern);
      });

      it('edit button click should load edit Courses page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Courses');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursesPageUrlPattern);
      });

      it('edit button click should load edit Courses page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Courses');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursesPageUrlPattern);
      });

      it('last delete button click should delete instance of Courses', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('courses').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', coursesPageUrlPattern);

        courses = undefined;
      });
    });
  });

  describe('new Courses page', () => {
    beforeEach(() => {
      cy.visit(`${coursesPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Courses');
    });

    it('should create an instance of Courses', () => {
      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        courses = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', coursesPageUrlPattern);
    });
  });
});
