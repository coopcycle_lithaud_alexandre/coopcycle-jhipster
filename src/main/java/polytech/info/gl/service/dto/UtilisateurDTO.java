package polytech.info.gl.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;
import polytech.info.gl.domain.enumeration.Role;

/**
 * A DTO for the {@link polytech.info.gl.domain.Utilisateur} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class UtilisateurDTO implements Serializable {

    @NotNull
    private Long id;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String nomUtilisateur;

    @NotNull
    @Min(value = 18L)
    private Long age;

    @NotNull
    private String addresse;

    @NotNull
    private Role role;

    private CoursesDTO courses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getAddresse() {
        return addresse;
    }

    public void setAddresse(String addresse) {
        this.addresse = addresse;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public CoursesDTO getCourses() {
        return courses;
    }

    public void setCourses(CoursesDTO courses) {
        this.courses = courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UtilisateurDTO)) {
            return false;
        }

        UtilisateurDTO utilisateurDTO = (UtilisateurDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, utilisateurDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UtilisateurDTO{" +
            "id=" + getId() +
            ", nomUtilisateur='" + getNomUtilisateur() + "'" +
            ", age=" + getAge() +
            ", addresse='" + getAddresse() + "'" +
            ", role='" + getRole() + "'" +
            ", courses=" + getCourses() +
            "}";
    }
}
