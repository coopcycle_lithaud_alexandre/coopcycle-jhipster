package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Panier;
import polytech.info.gl.service.dto.PanierDTO;

/**
 * Mapper for the entity {@link Panier} and its DTO {@link PanierDTO}.
 */
@Mapper(componentModel = "spring")
public interface PanierMapper extends EntityMapper<PanierDTO, Panier> {}
