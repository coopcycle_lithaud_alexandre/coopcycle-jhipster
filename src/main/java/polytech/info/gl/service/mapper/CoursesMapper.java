package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Courses;
import polytech.info.gl.service.dto.CoursesDTO;

/**
 * Mapper for the entity {@link Courses} and its DTO {@link CoursesDTO}.
 */
@Mapper(componentModel = "spring")
public interface CoursesMapper extends EntityMapper<CoursesDTO, Courses> {}
