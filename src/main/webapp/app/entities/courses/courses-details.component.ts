import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICourses } from '@/shared/model/courses.model';
import CoursesService from './courses.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CoursesDetails extends Vue {
  @Inject('coursesService') private coursesService: () => CoursesService;
  @Inject('alertService') private alertService: () => AlertService;

  public courses: ICourses = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.coursesId) {
        vm.retrieveCourses(to.params.coursesId);
      }
    });
  }

  public retrieveCourses(coursesId) {
    this.coursesService()
      .find(coursesId)
      .then(res => {
        this.courses = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
