package polytech.info.gl.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link polytech.info.gl.domain.Courses} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CoursesDTO implements Serializable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CoursesDTO)) {
            return false;
        }

        CoursesDTO coursesDTO = (CoursesDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, coursesDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CoursesDTO{" +
            "id=" + getId() +
            "}";
    }
}
