/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CoursesDetailComponent from '@/entities/courses/courses-details.vue';
import CoursesClass from '@/entities/courses/courses-details.component';
import CoursesService from '@/entities/courses/courses.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Courses Management Detail Component', () => {
    let wrapper: Wrapper<CoursesClass>;
    let comp: CoursesClass;
    let coursesServiceStub: SinonStubbedInstance<CoursesService>;

    beforeEach(() => {
      coursesServiceStub = sinon.createStubInstance<CoursesService>(CoursesService);

      wrapper = shallowMount<CoursesClass>(CoursesDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { coursesService: () => coursesServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCourses = { id: 123 };
        coursesServiceStub.find.resolves(foundCourses);

        // WHEN
        comp.retrieveCourses(123);
        await comp.$nextTick();

        // THEN
        expect(comp.courses).toBe(foundCourses);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCourses = { id: 123 };
        coursesServiceStub.find.resolves(foundCourses);

        // WHEN
        comp.beforeRouteEnter({ params: { coursesId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.courses).toBe(foundCourses);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
