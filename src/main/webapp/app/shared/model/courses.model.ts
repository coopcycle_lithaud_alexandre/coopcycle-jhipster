import { IUtilisateur } from '@/shared/model/utilisateur.model';

export interface ICourses {
  id?: number;
  utilisateurs?: IUtilisateur[] | null;
}

export class Courses implements ICourses {
  constructor(public id?: number, public utilisateurs?: IUtilisateur[] | null) {}
}
