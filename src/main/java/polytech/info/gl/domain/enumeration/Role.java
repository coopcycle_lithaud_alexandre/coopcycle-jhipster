package polytech.info.gl.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    Client,
    Societaire,
    Directeur_General,
    Commercant,
    Coursier,
    Banque,
    Fournisseur,
    Administrateur,
}
