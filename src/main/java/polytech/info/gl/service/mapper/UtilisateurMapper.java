package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Courses;
import polytech.info.gl.domain.Utilisateur;
import polytech.info.gl.service.dto.CoursesDTO;
import polytech.info.gl.service.dto.UtilisateurDTO;

/**
 * Mapper for the entity {@link Utilisateur} and its DTO {@link UtilisateurDTO}.
 */
@Mapper(componentModel = "spring")
public interface UtilisateurMapper extends EntityMapper<UtilisateurDTO, Utilisateur> {
    @Mapping(target = "courses", source = "courses", qualifiedByName = "coursesId")
    UtilisateurDTO toDto(Utilisateur s);

    @Named("coursesId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CoursesDTO toDtoCoursesId(Courses courses);
}
