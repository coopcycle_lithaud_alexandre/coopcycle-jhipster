import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Boutique e2e test', () => {
  const boutiquePageUrl = '/boutique';
  const boutiquePageUrlPattern = new RegExp('/boutique(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const boutiqueSample = { idBoutique: 65507 };

  let boutique;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/boutiques+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/boutiques').as('postEntityRequest');
    cy.intercept('DELETE', '/api/boutiques/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (boutique) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/boutiques/${boutique.id}`,
      }).then(() => {
        boutique = undefined;
      });
    }
  });

  it('Boutiques menu should load Boutiques page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('boutique');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Boutique').should('exist');
    cy.url().should('match', boutiquePageUrlPattern);
  });

  describe('Boutique page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(boutiquePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Boutique page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/boutique/new$'));
        cy.getEntityCreateUpdateHeading('Boutique');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', boutiquePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/boutiques',
          body: boutiqueSample,
        }).then(({ body }) => {
          boutique = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/boutiques+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/boutiques?page=0&size=20>; rel="last",<http://localhost/api/boutiques?page=0&size=20>; rel="first"',
              },
              body: [boutique],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(boutiquePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Boutique page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('boutique');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', boutiquePageUrlPattern);
      });

      it('edit button click should load edit Boutique page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Boutique');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', boutiquePageUrlPattern);
      });

      it('edit button click should load edit Boutique page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Boutique');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', boutiquePageUrlPattern);
      });

      it('last delete button click should delete instance of Boutique', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('boutique').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', boutiquePageUrlPattern);

        boutique = undefined;
      });
    });
  });

  describe('new Boutique page', () => {
    beforeEach(() => {
      cy.visit(`${boutiquePageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Boutique');
    });

    it('should create an instance of Boutique', () => {
      cy.get(`[data-cy="idBoutique"]`).type('84204').should('have.value', '84204');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        boutique = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', boutiquePageUrlPattern);
    });
  });
});
