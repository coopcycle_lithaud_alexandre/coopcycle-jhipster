import { ICourses } from '@/shared/model/courses.model';

import { Role } from '@/shared/model/enumerations/role.model';
export interface IUtilisateur {
  id?: number;
  nomUtilisateur?: string;
  age?: number;
  addresse?: string;
  role?: Role;
  courses?: ICourses | null;
}

export class Utilisateur implements IUtilisateur {
  constructor(
    public id?: number,
    public nomUtilisateur?: string,
    public age?: number,
    public addresse?: string,
    public role?: Role,
    public courses?: ICourses | null
  ) {}
}
